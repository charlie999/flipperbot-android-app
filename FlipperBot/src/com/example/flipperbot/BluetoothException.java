package com.example.flipperbot;

public class BluetoothException extends Exception
{
	private final String message;
	
	public BluetoothException (String message)
	{
		this.message = message;
	}
	
	@Override
	public String toString ()
	{
		return this.message;
	}

}
