package com.example.flipperbot;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Main class
 * @author Charles Hastie
 * @version 2.1
 */
public class MainActivity extends Activity
{
	private TextView bluetoothStatusField;
	private TextView connectionStatusField;
	private TextView nameField;
	private TextView addressField;
	private TextView dataField;
	private TextView timeoutField;
	
	private Button flipButton;
	private Button driveButton;
	
	private static final Handler mHandler = new Handler();
	
	private static final int REQUEST_BLUETOOTH_ENABLE = 0;
	
	private final String targetMAC = "00:06:66:4B:F2:1E";
	
	private long timeout = 5000;
	
	private double Heading;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		bluetoothStatusField = (TextView)findViewById(R.id.bluetooth_status);
		connectionStatusField = (TextView)findViewById(R.id.connection_status);
		nameField = (TextView)findViewById(R.id.device_name);
		addressField = (TextView)findViewById(R.id.device_address);
		dataField = (TextView)findViewById(R.id.data);
		timeoutField = (TextView)findViewById(R.id.timeout);
		
		flipButton = (Button)findViewById(R.id.flip);
		driveButton = (Button)findViewById(R.id.drive);
		
		getWindow ().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		BluetoothManager.instance.intialize(this);
		
		setupButtons ();
	}
	
	@Override
	protected void onResume ()
	{
		super.onResume();
		
		Runnable uiThread = new Runnable ()
		{
			@Override
			public void run()
			{
				updateUI ();
				
				try {
					long time = BluetoothManager.instance.getTimeSinceLastMessage(targetMAC);
					timeoutField.setText(String.valueOf(time));
					if (time > timeout)
						BluetoothManager.instance.disconnect(targetMAC);
				} catch (BluetoothException e) {
					timeoutField.setText(R.string.No_value);
				}
				mHandler.postDelayed(this, 50);
			}

			private void updateUI()
			{
				bluetoothStatusField.setText(String.valueOf(getBluetoothStatus()));
				connectionStatusField.setText(String.valueOf(BluetoothManager.instance.isConnected()));
				
				if (BluetoothManager.instance.isConnected(targetMAC))
				{
					nameField.setText(BluetoothManager.instance.getConnctedDevices().get(0).getName());
					addressField.setText(BluetoothManager.instance.getConnctedDevices().get(0).getAddress());
					flipButton.setEnabled(true);
					driveButton.setEnabled(true);
				}
				else
				{
					flipButton.setEnabled(false);
					driveButton.setEnabled(false);
					nameField.setText(R.string.Not_connected);
					addressField.setText(R.string.Not_connected);
				}
			}
		};
		
		Runnable controlThread = new Runnable()
		{
			final long period = 100;
			
			@Override
			public void run()
			{
				if (BluetoothManager.instance.isConnected(targetMAC))
				{
					try {
						String data = BluetoothManager.instance.readMessage(targetMAC);
						if (data != null)
							dataField.setText(parseData(data));
						
						//do control stuff here.
						
						report ();
					} catch (BluetoothException e) {
						dataField.setText("");
					}
				}
				mHandler.postDelayed(this, period);
			}
		};
		mHandler.post(uiThread);
		mHandler.post(controlThread);
	}
	
	private String parseData(String data)
	{
		String command = data.split(":")[0];
		String[] values = data.split(":")[1].split(" ");
		String output = new String ();
		
		if (command.equals(CommandLibrary.POLL))
		{
			//
		}
		else if (command.equals(CommandLibrary.REPORT))
		{
			Heading += Double.valueOf(values[0]);
			Heading /= 2;
			output += command;
			output += "\nAngle:\t";
			output += String.valueOf(values[0]);
			output += "\tDegrees\nTime:\t";
			output += String.valueOf(values[1]);
			output += "\tmiliSeconds";
		}
		else
			output = "Unknown Command:\n" + data;
			
		return output;
	}
	

	@Override
	protected void onPause ()
	{
		super.onPause();
	}
	
	@Override
	public void onStop ()
	{
		super.onStop();
	}
	
	@Override
	public void onDestroy ()
	{
		BluetoothManager.instance.disconnect();
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu (Menu menu)
	{
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.menu_activate).setEnabled(!getBluetoothStatus());
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected (MenuItem item)
	{
		invalidateOptionsMenu();
		switch (item.getItemId())
		{
		case R.id.menu_activate:
			activateBluetooth();
			return true;
			
		case R.id.menu_info:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.Bluetooth_info);
//			builder.setMessage("Hello.");
	        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
	        builder.create().show();
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case REQUEST_BLUETOOTH_ENABLE:
			if (resultCode == RESULT_OK)
				Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show ();
			else
				Toast.makeText(this, "Failure", Toast.LENGTH_SHORT).show ();
			break;
		}
	}
	
	public boolean activateBluetooth ()
	{
		if (!getBluetoothStatus())
		{
			Intent bluetoothIntent = new Intent (BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(bluetoothIntent, REQUEST_BLUETOOTH_ENABLE);
		}
		return getBluetoothStatus();
	}

	private boolean getBluetoothStatus()
	{
		return BluetoothAdapter.getDefaultAdapter().isEnabled();
	}
	
	private void poll () throws BluetoothException
	{
		String temp = CommandLibrary.getMessage(CommandLibrary.POLL,
				new String[] {String.valueOf(System.currentTimeMillis())});
		BluetoothManager.instance.sendMessage(temp, targetMAC);
	}
	
	private void report () throws BluetoothException
	{
		String temp = CommandLibrary.getMessage(CommandLibrary.REPORT);
		BluetoothManager.instance.sendMessage(temp, targetMAC);
	}
	
	private void throttle (int value) throws BluetoothException
	{
		String temp = CommandLibrary.getMessage(CommandLibrary.THROTTLE,
				new String[] {String.valueOf(value)});
		BluetoothManager.instance.sendMessage(temp, targetMAC);
	}
	
	@SuppressWarnings("unused")
	private void rotate (double heading) throws BluetoothException
	{
		String temp = CommandLibrary.getMessage(CommandLibrary.ROTATE,
				new String[] {String.valueOf(heading)});
		BluetoothManager.instance.sendMessage(temp, targetMAC);
	}
	
	private void actuate (int number, boolean set) throws BluetoothException
	{
		String temp = CommandLibrary.getMessage(CommandLibrary.ACTUATE,
				new String[] {String.valueOf(number), String.valueOf(set)});
		BluetoothManager.instance.sendMessage(temp, targetMAC);
	}
	
	private void setupButtons ()
	{
		flipButton.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View view, MotionEvent event)
			{
				if (event.getAction() == MotionEvent.ACTION_DOWN)
				{
					try {
						actuate (0, true);
					} catch (BluetoothException e) {}
				}
				else if (event.getAction() == MotionEvent.ACTION_UP)
				{
					try {
						actuate (0, false);
					} catch (BluetoothException e) {}
				}
				return false;
			}
		});
		driveButton.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View view, MotionEvent event)
			{
				if (event.getAction() == MotionEvent.ACTION_DOWN)
				{
					try {
						throttle (100);
					} catch (BluetoothException e) {}
				}
				else if (event.getAction() == MotionEvent.ACTION_UP)
				{
					try {
						throttle (0);
					} catch (BluetoothException e) {}
				}
				return false;
			}
		});
	}
	
	@SuppressWarnings("unused")
	private void popupMessage (final String message)
	{
		this.runOnUiThread(new Runnable ()
		{
			@Override
			public void run()
			{
				Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();		
			}
		});
	}
}