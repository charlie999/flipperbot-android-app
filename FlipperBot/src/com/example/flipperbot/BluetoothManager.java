package com.example.flipperbot;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.PriorityBlockingQueue;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

/**
 * Bluetooth control class
 * Automatically accepts connections from external devices and manages threads to read data from attached devices
 * @version 2.1
 * @author Charles Hastie
 */
public class BluetoothManager
{
	public static BluetoothManager instance = new BluetoothManager();
	private boolean initialized = false;
	
	private final static String DEFAULT_NAME = "BluetoothManager";
 
	private final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	private ServerThread serverThread;
	private final ArrayList<BTConnection> connectionList = new ArrayList<BTConnection> ();
	
	private BluetoothManager() {}
	
	public void intialize(final Activity activity)
	{
		// proceed asynchronously to wait for the user to activate BT
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				while (!startBluetoothThread()) {}
				initialized = true;
			}
		}).start();
	}

	public boolean isInitialized()
	{
		return initialized;
	}
	
	public ArrayList<BluetoothDevice> getConnctedDevices ()
	{
		ArrayList<BluetoothDevice> temp = new ArrayList<BluetoothDevice> ();
		for (BTConnection btConnection : connectionList)
			temp.add(btConnection.getDevice());
		return temp;
	}
	
	public boolean isConnected ()
	{
		return !connectionList.isEmpty();
	}
	
	public boolean isConnected (final BluetoothDevice device)
	{
		for (BTConnection btConnection : connectionList)
			if (btConnection.getDevice().equals(device))
				return true;
		return false;
	}
	
	public boolean isConnected (final String MAC)
	{
		for (BTConnection btConnection : connectionList)
			if (btConnection.getDevice().getAddress().equals(MAC))
				return true;
		return false;
	}
	
	public void sendMessage (String message) throws BluetoothException
	{
		if (!isConnected ())
			throw (new BluetoothException ("Not connected."));
		for (BTConnection btConnection : connectionList)
			btConnection.send(message);
	}
	
	public void sendMessage (String message, final BluetoothDevice device) throws BluetoothException
	{
		if (!isConnected (device))
			throw (new BluetoothException ("Device not found: " + device.getName() + "."));
		for (BTConnection btConnection : connectionList)
			if (btConnection.getDevice().equals(device))
				btConnection.send(message);
	}
	
	public void sendMessage (String message, final String MAC) throws BluetoothException
	{
		if (!isConnected (MAC))
			throw (new BluetoothException ("Device not found: " + MAC + "."));
		for (BTConnection btConnection : connectionList)
			if (btConnection.getDevice().getAddress().equals(MAC))
				btConnection.send(message);
	}
	
	public String readMessage (final BluetoothDevice device) throws BluetoothException
	{
		for (BTConnection btConnection : connectionList)
			if (btConnection.getDevice().equals(device))
				return btConnection.read();
		throw (new BluetoothException ("Device not found: " + device.getName() + "."));
	}
	
	public String readMessage (final String MAC) throws BluetoothException
	{
		for (BTConnection btConnection : connectionList)
			if (btConnection.getDevice().getAddress().equals(MAC))
				return btConnection.read();
		throw (new BluetoothException ("Device not found: " + MAC + "."));
	}
	
	public long getTimeSinceLastMessage () throws BluetoothException
	{
		if (!isConnected ())
			throw (new BluetoothException ("No device connected."));
		long time = 0;
		for (BTConnection btConnection : connectionList)
			if (btConnection.getLastMessageTime() > time)
				time = btConnection.getLastMessageTime();
		return System.currentTimeMillis() - time;
	}
	
	public long getTimeSinceLastMessage (final BluetoothDevice device) throws BluetoothException
	{
		for (BTConnection btConnection : connectionList)
			if (btConnection.getDevice().equals(device))
				return System.currentTimeMillis() - btConnection.getLastMessageTime();
		throw (new BluetoothException ("Device not found: " + device.getName() + "."));
	}
	
	public long getTimeSinceLastMessage (final String MAC) throws BluetoothException
	{
		for (BTConnection btConnection : connectionList)
			if (btConnection.getDevice().getAddress().equals(MAC))
				return System.currentTimeMillis() - btConnection.getLastMessageTime();
		throw (new BluetoothException ("Device not found: " + MAC + "."));
	}
	
	public void disconnect ()
	{
		try {
			serverThread.disconnect();
			for (BTConnection btConnection : connectionList)
				btConnection.disconnect();
		} catch (IOException e) {}
	}
	
	public void disconnect (final BluetoothDevice device) throws BluetoothException
	{
		try {
			for (BTConnection btConnection : connectionList)
				if (btConnection.getDevice().equals(device))
					btConnection.disconnect();
			throw (new BluetoothException ("Device not connected: " + device.getName() + "."));
		} catch (IOException e) {}
	}
	
	public void disconnect (final String MAC) throws BluetoothException
	{
		try {
			for (BTConnection btConnection : connectionList)
				if (btConnection.getDevice().getAddress().equals(MAC))
					btConnection.disconnect();
			throw (new BluetoothException ("Device not connected: " + MAC + "."));
		} catch (IOException e) {}
	}	
	
	private boolean startBluetoothThread()
	{
		try {
			serverThread = new ServerThread (DEFAULT_NAME,
					UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
			serverThread.start();
			return true; 
		} catch (IOException e) {
			serverThread = null;
			return false; 
		}
	}
	
	private void manageConnection (BluetoothSocket socket)
	{
		BTConnection socketThread = new BTConnection (socket);
		//detect duplicate connections in the connection list and close/remove them
		for (BTConnection btConnection : connectionList)
		{
			if (btConnection.getDevice().equals(socket.getRemoteDevice()))
			{
				try {
					btConnection.disconnect();
					connectionList.remove(btConnection);
				} catch (IOException e) {}
			}
		}
		connectionList.add(socketThread);
	}
	
	private class ServerThread extends Thread
	{
		private final BluetoothServerSocket mBluetoothServerSocket;
		
		public ServerThread (String name, UUID uuid) throws IOException
		{
			mBluetoothServerSocket = mBluetoothAdapter.
					listenUsingRfcommWithServiceRecord(name, uuid);
		}
		
		@Override
		public void run ()
		{
			while (true)
			{
				try {
					BluetoothSocket mBluetoothSocket = mBluetoothServerSocket.accept();
					if (mBluetoothSocket != null)
						manageConnection (mBluetoothSocket);
				} catch (IOException e) {
					break;
				}
			}
		}
		
		public void disconnect () throws IOException
		{
			this.mBluetoothServerSocket.close();
		}
	}
	
	private class BTConnection
	{
		private final BluetoothSocket mBluetoothSocket;
		private final BluetoothDevice mBluetoothDevice;
		
		private BTReadThread readThread;
		private BTSendThread sendThread;
		
		private final long connectionTime;
		private long timeout;
		
		public BTConnection (BluetoothSocket socket)
		{
			mBluetoothSocket = socket;
			mBluetoothDevice = socket.getRemoteDevice();
			connectionTime = System.currentTimeMillis();
			timeout = System.currentTimeMillis();
			try {
				readThread = new BTReadThread (socket.getInputStream());
				sendThread = new BTSendThread (socket.getOutputStream());
				readThread.start();
				sendThread.start();
			} catch (IOException e) {}
		}
		
		public void send (String message)
		{
			sendThread.outgoing.add(message);
		}
		
		public String read ()
		{
			return readThread.incoming.poll();
		}
		
		@SuppressWarnings("unused")
		public long getConnectionTime ()
		{
			return connectionTime;
		}
		
		public long getLastMessageTime ()
		{
			return timeout;
		}
		
		public void disconnect () throws IOException
		{
			mBluetoothSocket.close();
		}
		
		@SuppressWarnings("unused")
		public BluetoothSocket getSocket ()
		{
			return this.mBluetoothSocket;
		}
		
		public BluetoothDevice getDevice ()
		{
			return this.mBluetoothDevice;
		}
		
		private class BTSendThread extends Thread
		{
			private final OutputStream out;
			public final PriorityBlockingQueue<String> outgoing = new PriorityBlockingQueue<String>();
			
			BTSendThread (final OutputStream out)
			{
				this.out = out;
			}
			
			@Override
			public void run ()
			{
				while (true)
				{
					try {
						out.write(outgoing.take().getBytes());
					} catch (IOException e) {
						break;
					} catch (InterruptedException e) {}
				}
			}
		}
		
		private class BTReadThread extends Thread
		{
			private final InputStream in;
			public final PriorityBlockingQueue<String> incoming = new PriorityBlockingQueue<String>();
			
			BTReadThread (final InputStream in)
			{
				this.in = in;
			}
			
			@Override
			public void run ()
			{
				String message = new String ();
				while (true)
				{
					char c;
					try {
						c = (char) in.read();
					} catch (IOException e) {
						break;
					}
					
					if (c != '\n')
						message += String.valueOf(c);
					else
					{
						incoming.add(message);
						timeout = System.currentTimeMillis();
						message = new String ();
					}
				}
			}
		}
	}
}