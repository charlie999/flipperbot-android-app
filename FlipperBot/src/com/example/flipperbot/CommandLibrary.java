package com.example.flipperbot;

/**
 * Bluetooth Command lookup class
 * @author Charles Hastie
 * @version 2.1
 */
public class CommandLibrary
{
	public static final String POLL = "POLL";
	public static final String REPORT = "REPORT";
	public static final String ROTATE = "ROTATE";
	public static final String THROTTLE = "THROTTLE";
	public static final String ACTUATE = "ACTUATE";
	private static final String ON = "ON";
	private static final String OFF = "OFF";
	
	public static String getMessage (String command)
	{
		String temp = new String ();
		if (command.equals(REPORT))
			temp = getReportMessage ();
		return temp;
	}
	
	public static String getMessage (String command, String[] data)
	{
		String temp = new String ();
		if (command.equals(POLL))
			temp = getPollMessage (data[0]);
		else if (command.equals(ROTATE))
			temp = getRotateMessage (data[0]);
		else if (command.equals(THROTTLE))
			temp = getThrottleMessage (data[0]);
		else if (command.equals(ACTUATE))
			temp = getActuateMessage (data[0], data[1]);
		return temp;
	}
	
	private static String getPollMessage (String data)
	{
		String temp = new String ();
		temp += POLL + ":";
		temp += String.valueOf(System.currentTimeMillis());
		temp += "\n";
		return temp;
	}
	
	private static String getReportMessage ()
	{
		return REPORT + ":\n";
	}
	
	private static String getRotateMessage (String heading)
	{
		return ROTATE + ":" + heading + "\n";
	}
	
	private static String getThrottleMessage (String value)
	{
		return THROTTLE + ":" + value + "\n";
	}
	
	private static String getActuateMessage (String number, String status)
	{
		String temp = ACTUATE + ":" + number + " ";
		if (status.equals(String.valueOf(true)))
			temp += ON;
		else
			temp += OFF;
		temp += "\n";
		return temp;
	}
}
